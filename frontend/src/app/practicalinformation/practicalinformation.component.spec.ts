import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticalinformationComponent } from './practicalinformation.component';

describe('PracticalinformationComponent', () => {
  let component: PracticalinformationComponent;
  let fixture: ComponentFixture<PracticalinformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticalinformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticalinformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
