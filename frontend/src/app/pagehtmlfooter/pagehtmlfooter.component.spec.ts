import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagehtmlfooterComponent } from './pagehtmlfooter.component';

describe('PagehtmlfooterComponent', () => {
  let component: PagehtmlfooterComponent;
  let fixture: ComponentFixture<PagehtmlfooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagehtmlfooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagehtmlfooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
